#include <ros/ros.h>
#include "geom_inertia_estimator.h"

int main(int argc, char *argv[]) {
  ros::init(argc, argv, "geom_inertia_estimator");
  ros::NodeHandle nhtopics("");
  ros::NodeHandle nhparams("~");
  InertiaEstimator estimator(nhtopics, nhparams);
  ros::spin();
  return 0;
}
