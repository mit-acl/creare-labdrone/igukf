#ifndef GEOMINERTIAESTIMATOR_H
#define GEOMINERTIAESTIMATOR_H

#include <cmath>
#include <strstream>
#include <queue>
#include <vector>

#include <ros/ros.h>

#include <Eigen/Eigen>

// #include <geometry_msgs/PoseWithCovarianceStamped.h>
#include <geometry_msgs/PoseStamped.h>
// #include <geometry_msgs/Point.h>
// #include <geometry_msgs/WrenchStamped.h>
// #include <sensor_msgs/Imu.h>
#include <snapstack_msgs/IMU.h>
#include <snapstack_msgs/Motors.h>

// #include <igukf/MotorRPM.h>
#include <igukf/ParameterEstimates.h>

#include "v_tools.h"

using namespace std;

// define state & measurement sizes
#define NUM_STATES         int(31)
#define NUM_STATES_TANGENT int(30)
#define NUM_MEAS_POSE      int(7)
#define NUM_MEAS_IMU       int(6)
#define NUM_SIGMAS         int(2*(NUM_STATES_TANGENT)+1)
#define RT_PI  3.14159265358979323846
#define RT_PIF 3.1415927F

// structs/classes to cleanly save sates, inputs, measurements, and initialization values
struct InitVars
{
  // variables needed for filter initialization
  // flags
  bool readyToInitialize = false;
  bool poseInitialized = false;
  bool imuInitialized = false;
  bool rpmInitialized = false;
  // time
  ros::Time t;
  // pose
  vec3 r;
  quat q;
  // imu
  vec3 Omega;
  // biases
  vec3 b_a;
  vec3 b_Omega;
  // from param file - user set
  double m;
  mat3 I;
  vec3 r_BM;
  vec3 r_BP;
  vec2 r_BI;
  // uncertainty covariance
  Eigen::Matrix<flt,NUM_STATES_TANGENT,NUM_STATES_TANGENT> P0;
};

class State
{
public:
  // state variables
  // trajectory
  vec3 r;
  vec3 v;
  quat q;
  vec3 Omega;
  // inertia
  double m;
  mat3 I;
  vec3 r_BM;
  // geometric
  vec3 r_BP;
  vec2 r_BI;
  // biases
  vec3 b_a;
  vec3 b_Omega;

  // state manifold operations to "add" & "subtract" states
  Eigen::Matrix<flt, NUM_STATES_TANGENT, 1> boxminus(const State& other) const;
  void boxplus(const Eigen::Matrix<flt, NUM_STATES_TANGENT, 1> &delta);
};

struct StateWithCov
{
  // complete state with covariance and timestamp
  State X;
  Eigen::Matrix<flt,NUM_STATES_TANGENT,NUM_STATES_TANGENT> P;
  ros::Time t;
};

struct Input
{
  // inputs for prediction step
  vec4 rpm_sq;
  Eigen::Matrix<flt, Eigen::Dynamic, 1> pwm;
  Eigen::Matrix<flt, Eigen::Dynamic, 1> f;
  Eigen::Matrix<flt, Eigen::Dynamic, 1> tau;
  ros::Time t;
};

struct MeasPose
{
  // measurements for pose measurement update
  vec3 r;
  quat q;
  mat6 cov;
};

struct MeasImu
{
  // measurements for IMU measurement update
  vec3 a;
  vec3 Omega;
};

struct Consts
{
  // often used constants
  // covariance IMU
  mat6 RImu;

  // process model covariance
  Eigen::Matrix<flt,NUM_STATES_TANGENT,NUM_STATES_TANGENT> Qx;

  // rotor thrust and drag moment coefficients
  flt k_f,k_M;
  // matrices for wrench calculation from rotor rpms
  Eigen::Matrix<flt,3,4> P_f,P_M;
  int num_rotors;

  // attitude IMU & pose sensor
  Eigen::Matrix<flt,3,3> R_BI, R_BP;
  quat q_RB;

  // UKF tuning parameters
  flt alpha, kappa, beta;
  flt lambda;
  // UKF weight matrices
  Eigen::Matrix<flt,2*NUM_STATES_TANGENT+1,1> Wm,Wc;

  // generally often used constants
  vec3 e_z;
  flt g;
  // matrices to speed up calculation
  mat3 rotate3;
  mat6 R_BP_6D;
  mat3 zero3;
  mat3 eye3;
  Eigen::Matrix<flt,NUM_STATES_TANGENT,NUM_STATES_TANGENT> eye_NST;
};


// estimator class
class InertiaEstimator
{
public:
  // node initialization function
  InertiaEstimator(const ros::NodeHandle& nh, const ros::NodeHandle& nhp);

  void resetFilter();
private:
  ros::NodeHandle nh_, nhp_;
  // subscribers and publisher
  ros::Subscriber sub_imu_, sub_odom_, sub_rpm_;
  ros::Publisher pub_estimates_;
  // member variables
  Consts consts_;
  InitVars init_vars_;
  StateWithCov state_;
  Input input_;
  std::queue<Input> inputList_;
  std::queue<MeasImu> measImuList_;
  double F_temp_[NUM_STATES_TANGENT*NUM_STATES_TANGENT];
  Eigen::Matrix<flt,NUM_STATES,NUM_SIGMAS> propagatedSigmaPointsMat_;

  /// \brief Maps from motor wrench to body wrench
  using Wrench = Eigen::Matrix<flt, 6, 1>;
  using WrenchMap = Eigen::Matrix<flt, 6, Eigen::Dynamic>;
  WrenchMap wrench_from_motor_thrust_map_;
  WrenchMap wrench_from_motor_torque_map_;
  int nrMotors_; ///< number of motors

  std::vector<double> polyF_; ///< thrust curve polynomial coefficients
  std::vector<double> polyT_; ///< torque curve polynomial coefficients

  // status flags
  bool initialized_ = false;

  // estimator member function to initialize, predict and update
  void initializeFilter(const InitVars& init);
  void predictUKF(StateWithCov &oldState, Input &input);
  void measUpdatePoseUKF(StateWithCov &state, MeasPose &meas);
  void measUpdateImuUKF(StateWithCov& state, Input& input, MeasImu& meas);

  // models
  void processModel(State& X, Input& U, flt &dt);
  Eigen::Matrix<flt,3,1> measurementModelPosition(State& X);
  Eigen::Matrix<flt,3,1> measurementModelAcceleration(State& X, Input &U);
  Wrench getWrench(const State& X, const Input& U);

  // UKF functions
  void getSigmaPoints(State* sigmaPoints, StateWithCov &state);
  void calculateMeanStateSigma(State &mean, const State sigmaPoints[NUM_SIGMAS]);

  // subscirber functions: messge conversion functions
  // void rpmMsg2input(Input &returnVar, const geom_inertia_estimator::MotorRPM::ConstPtr &msg);
  MeasPose poseMsg2measPose(const geometry_msgs::PoseStamped::ConstPtr &msg);
  MeasImu imuMsg2measImu(const snapstack_msgs::IMU::ConstPtr &msg);

  void pwm_callback(const snapstack_msgs::Motors::ConstPtr &msg);
  //void rpm_callback (const geometry_msgs::WrenchStamped::ConstPtr &msg);
  void pose_callback(const geometry_msgs::PoseStamped::ConstPtr &msg);
  void imu_callback(const snapstack_msgs::IMU::ConstPtr &msg);
  double evalPoly(const std::vector<double>& coeffs, double x);

  // publisher function
  void publishEstimates(const StateWithCov &estimate);
};

#endif // INERTIAESTIMATOR_H
